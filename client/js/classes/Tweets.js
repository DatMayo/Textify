class Tweets {
    constructor(API_URL) {
        this.offset = 0;
        this.api = API_URL;
        this.dom = new DOM();
        this.loading = false;
        this.end = false;
    }

    load(cookie) {
        this.loading = true;
        $.ajaxSetup({
            headers: {
                'token': cookie,
            }
        });
        $.get(`${this.api}tweets/${this.offset}`, (data) => {
                if (data.Error) {
                    // ToDo
                } else {
                    if (data.Success.Data.length > 0) {
                        data.Success.Data.forEach(tweet => {
                            this.dom.addTweetToDOM(tweet.text, tweet.account.username, tweet.createdAt);
                        });
                        this.offset += 10;
                    } else {
                        this.end = true;
                        this.offset -= 10;
                    }
                }
                this.loading = false;
            })
            .catch(() => {
                $('#error').modal('open');
            });
    }

    isLoading() {
        return this.loading;
    }

    isEnd() {
        return this.end;
    }
}