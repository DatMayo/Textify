class Account {
    constructor(API_URL) {
        this.api = API_URL;
    }
    login() {
        $.post(`${this.api}account/login`, {
                    username: $('#l-username').val(),
                    password: $('#l-password').val(),
                },
                (data) => {
                    if (data.Error) {
                        // ToDo
                    } else {
                        window.location.reload();
                    }
                })
            .fail(e => {
                let error = JSON.parse(e.responseText)['Error'];
                M.toast({
                    html: `There was an error during login.<br><br>Field: ${(error.Messages[0].param) ? error.Messages[0].param : 'other'}<br>Error: ${error.Messages[0].msg}`,
                });
            });
    }
    register() {
        $.post(`${this.api}account/login`, {
                    username: $('#l-username').val(),
                    password: $('#l-password').val(),
                },
                (data) => {
                    if (data.Error) {
                        // ToDo
                    } else {
                        cookie.setCookie('token', data.Success.Data.token);
                        $('#l-username').val('');
                        $('#l-password').val('');
                        $('#login').modal('close');
                        M.toast({
                            html: `Login successfull!`,
                        });
                        $('#tweets').empty();
                    }
                })
            .fail(e => {
                let error = JSON.parse(e.responseText)['Error'];
                M.toast({
                    html: `There was an error during login.<br><br>Field: ${(error.Messages[0].param) ? error.Messages[0].param : 'other'}<br>Error: ${error.Messages[0].msg}`,
                });
            });
    }
}