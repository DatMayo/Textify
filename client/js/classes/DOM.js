class DOM {
    addTweetToDOM(tweet, createdBy, createdAt) {
        $('#tweets').append(
            `<div class="row"><div class="col s12 m12 l12"><div class="card"><div class="card-content">${tweet}</div><div class="card-action card-footer">Written by <i>${createdBy}</i> at <i>${createdAt}</i>
					</div>
				</div>
			</div>
		</div>`
        );
    }
}