$(() => {
    const API_URL = 'http://localhost:8181/';

    // Initialization
    $('#error').modal();
    $('#login').modal();
    $('#register').modal();
    $('.account-dropdown').dropdown();
    $('.sidenav').sidenav({ edge: 'right', inDuration: 500, outDuration: 500 });

    const cookie = new Cookie();
    const account = new Account(API_URL);
    const tweets = new Tweets(API_URL);
    tweets.load(cookie.getCookie('token'));

    $(window).scroll(() => {
        if (!tweets.isLoading() && !tweets.isEnd())
            if (($(document).height() - $(this).height() - 150 <= $(this).scrollTop())) {
                tweets.load(cookie.getCookie('token'));
            }
    });

    // Login

    function openLogin() {
        $('#login').modal('open');
    }

    $('.login').on('click', () => {
        openLogin();
    });
    $('#login-a').on('click', () => {
        if (!$('#l-username').val()) {
            $('#l-username').effect('highlight', { color: '#f44336' });
            return $('#l-username').focus();
        }
        if (!$('#l-password').val()) {
            $('#l-password').effect('highlight', { color: '#f44336' });
            return $('#l-password').focus();
        }
        account.login();
    });

    // Registration

    function openRegistration() {
        $('#register').modal('open');
    }

    $('.register').on('click', () => {
        openRegistration();
    });

    $('#register-a').on('click', () => {
        if (!$('#r-username').val()) {
            $('#r-username').effect('highlight', { color: '#f44336' });
            return $('#r-username').focus();
        }
        if (!$('#r-password').val()) {
            $('#r-password').effect('highlight', { color: '#f44336' });
            return $('#r-password').focus();
        }
        if (!$('#r-password-retype').val()) {
            $('#r-password-retype').effect('highlight', { color: '#f44336' });
            return $('#r-password-retype').focus();
        }
        account.register()
    });
});