const Logger = require('../utils/logger');

class SessionManager
{
	/**
	 * Creates an instance of the session manager
	 * @param  {object} db Database handle
	 * @param  {string} token User-Token
	 */
	constructor(db, token)
	{
		this.db = db;
		this.token = token;
		this.sessionData = undefined;
	}
	/**
	 * Checks if the session exists
	 * @returns {boolean} Returns true if the session exists
	 */
	async checkSession()
	{
		if(this.token === undefined || this.token === null)
			return false;
		try
		{
			this.sessionData = await this.db.sessions.findOne(
				{
					where:
					{
						token: this.token,
					},
				});
			if(!this.sessionData)
				return false;
			return true;
		}
		catch(err)
		{
			Logger.log(err, 'error');
			return false;
		}
	}

	/**
	 * Returns the id of the cuser
	 * @returns {number} User SQL AccountID
	 */
	async getSessionUserId()
	{
		try
		{
			const accountData = await this.db.sessions.findOne(
				{
					attributes: ['accountId'],
					where:
					{
						token: this.token,
					},
				});
			if(!accountData)
				return -1;
			return parseInt(accountData.dataValues.accountId);
		}
		catch (err)
		{
			Logger.log(err, 'error');
			return -1;
		}
	}

	/**
	 * Updates the user session
	 * @returns {boolean} Returns true if update was successfull
	 */
	async updateSession()
	{
		const validUntil = new Date(Date.now() + (30 * 60 * 1000));
		try
		{
			await this.sessionData.update(
				{
					validUntil: validUntil,
				});
			return true;
		}
		catch(err)
		{
			Logger.log(err, 'error');
			return false;
		}
	}
	/**
	 * Returns that we're missing a token
	 * @param {object} res Express response object
	 */
	sendIncorrectOrMissingToken(res)
	{
		res.status(403).json(
			{
				'Error':
					{
						'Code': 403,
						'Messages': [
							{
								'msg': 'Incorrect or missing token',
							},
						],
					},
			});
	}

	/**
	 * Returns that we're missing a token
	 * @param {object} res Express response object
	 */
	sendInternalServerError(res)
	{
		return res.status(500).json(
			{
				'Error':
					{
						'Code': 500,
						'Messages': [
							{
								'msg': 'Internal server error',
							},
						],
					},
			});
	}
}

module.exports = SessionManager;