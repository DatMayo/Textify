const SessionManager = require('../../utils/sessionManager');

module.exports = (app, db) =>
{
	app.get('/t/:tweetId?', async (req, res) =>
	{
		const sessionManager = new SessionManager(db, req.header('token'));
		if(await sessionManager.checkSession() === false)
			return sessionManager.sendIncorrectOrMissingToken(res);

		if(await sessionManager.updateSession() === false)
			return sessionManager.sendInternalServerError(res);

		db.tweet.findOne(
			{
				where:
					{
						id: req.params.tweetId,
					},
				include: [
					{
						model: db.account,
						attributes: ['username', 'name', 'verified', 'avatarurl'],
					}],
				attributes: ['text', 'createdAt'],
			})
			.then(tweetMessage =>
			{
				if(!tweetMessage)
				{
					return res.status(404).json(
						{
							'Error':
							{
								'Code': 404,
								'Messages': [
									{
										'msg': 'Tweet not found',
									},
								],
							},
						});
				}
				res.status(200).json(
					{
						'Success':
							{
								'Code': 200,
								'Data': tweetMessage,
							},
					});
			});
	});

	app.get('/tweets/:offset?', async (req, res) =>
	{
		if(!req.params.offset)
			req.params.offset = 0;
		if(req.header('token'))
		{
			const sessionManager = new SessionManager(db, req.header('token'));
			if(await sessionManager.checkSession() === false)
				return sessionManager.sendIncorrectOrMissingToken(res);

			if(await sessionManager.updateSession() === false)
				return sessionManager.sendInternalServerError(res);
			db.follower.findAll(
				{
					attributes: ['followerId'],
					where:
					{
						followeeId: await sessionManager.getSessionUserId(),
					},
				})
				.then(data =>
				{
					const following = [];
					data.forEach(element =>
					{
						following.push({ accountId: element.dataValues.followerId });
					});
					const Op = db.Sequelize.Op;
					db.tweet.findAll(
						{
							attributes: ['text', 'createdAt'],
							include: [
								{
									model: db.account,
									attributes: ['username', 'name', 'verified', 'avatarurl'],
								}],
							limit: 10,
							offset: parseInt(req.params.offset),
							order:
							[
								[ 'id', 'DESC' ],
							],
							where:
							{
								[Op.or]: following,
							},
						})
						.then(tweetMessages =>
						{
							if(!tweetMessages)
							{
								return res.status(404).json(
									{
										'Error':
										{
											'Code': 404,
											'Messages': [
												{
													'msg': 'Tweet not found',
												},
											],
										},
									});
							}
							res.status(200).json(
								{
									'Success':
									{
										'Code': 200,
										'Data': tweetMessages,
									},
								});
						});
				});
		}
		else
		{
			db.tweet.findAll(
				{
					attributes: ['text', 'createdAt'],
					include: [
						{
							model: db.account,
							attributes: ['username', 'name', 'verified', 'avatarurl'],
						}],
					limit: 10,
					offset: parseInt(req.params.offset),
					order:
					[
						[ 'id', 'DESC' ],
					],
				})
				.then(tweetMessages =>
				{
					if(!tweetMessages)
					{
						return res.status(404).json(
							{
								'Error':
								{
									'Code': 404,
									'Messages': [
										{
											'msg': 'Tweet not found',
										},
									],
								},
							});
					}
					res.status(200).json(
						{
							'Success':
							{
								'Code': 200,
								'Data': tweetMessages,
							},
						});
				});
		}
	});
};