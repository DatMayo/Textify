const { check, validationResult } = require('express-validator/check');
const SessionManager = require('../../utils/sessionManager');

module.exports = (app, db) =>
{
	app.post('/tweet',
		[
			check('message')
				.not().isEmpty()
				.isLength({ min: 2 }),
		], async (req, res) =>
		{
			const errors = validationResult(req);
			if(!errors.isEmpty())
			{
				return res.status(400).json(
					{
						'Error':
						{
							'Code': 400,
							'Messages': errors.array(),
						},
					});
			}
			const sessionManager = new SessionManager(db, req.header('token'));
			if(await sessionManager.checkSession() === false)
				return sessionManager.sendIncorrectOrMissingToken(res);

			if(await sessionManager.updateSession() === false)
				return sessionManager.sendInternalServerError(res);

			const userID = await sessionManager.getSessionUserId();
			db.tweet.create(
				{
					text: req.body.message,
					accountId: userID,
				})
				.then(tweet =>
				{
					delete tweet.dataValues['id'];
					delete tweet.dataValues['accountId'];
					delete tweet.dataValues['updatedAt'];
					res.status(200).json(
						{
							'Success':
							{
								'Code': 200,
								'Data': tweet,
							},
						});
				});
		});
};