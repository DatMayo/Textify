const SessionManager = require('../../utils/sessionManager');

module.exports = (app, db) =>
{
	app.get('/follow/:userid?', async (req, res) =>
	{
		const sessionManager = new SessionManager(db, req.header('token'));
		if(await sessionManager.checkSession() === false)
			return sessionManager.sendIncorrectOrMissingToken(res);

		if(await sessionManager.updateSession() === false)
			return sessionManager.sendInternalServerError(res);
		if(!req.params.userid)
		{
			return res.status(400).json(
				{
					'Error':
					{
						'Code': 400,
						'Messages': [
							{
								'msg': 'Missing parameter userid',
							},
						],
					},
				});
		}
		if(isNaN(parseInt(req.params.userid)))
		{
			return res.status(400).json(
				{
					'Error':
					{
						'Code': 400,
						'Messages': [
							{
								'msg': 'The given userid is not a number',
							},
						],
					},
				});
		}
		const myId = await sessionManager.getSessionUserId();
		if(myId == req.params.userid)
		{
			return res.status(400).json(
				{
					'Error':
					{
						'Code': 400,
						'Messages': [
							{
								'msg': 'You can\'t follow yourself ;)',
							},
						],
					},
				});
		}
		db.follower.findOrCreate(
			{
				defaults:
				{
					followeeId: myId,
					followerId: req.params.userid,
				},
				where:
				{
					followeeId: myId,
					followerId: req.params.userid,
				},
			})
			.then(([follow, created]) =>
			{
				if(!created)
				{
					return res.status(400).json(
						{
							'Error':
							{
								'Code': 400,
								'Messages': [
									{
										'msg': 'You\'re allready following this person',
									},
								],
							},
						});
				}
				else
				{
					res.status(200).json(
						{
							'Success':
							{
								'Code': 200,
								'Data': follow,
							},
						});
				}
			});
	});
};