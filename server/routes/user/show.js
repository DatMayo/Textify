const SessionManager = require('../../utils/sessionManager');

module.exports = (app, db) =>
{
	app.get('/u/:username?', async (req, res) =>
	{
		const sessionManager = new SessionManager(db, req.header('token'));
		if(await sessionManager.checkSession() === false)
			return sessionManager.sendIncorrectOrMissingToken(res);

		if(await sessionManager.updateSession() === false)
			return sessionManager.sendInternalServerError(res);


		if(!req.params.username)
		{
			return res.status(400).json(
				{
					'Error':
					{
						'Code': 400,
						'Messages': [
							{
								'msg': 'Missing parameter username',
							},
						],
					},
				});
		}
		db.account.findOne(
			{
				where:
					{
						username: req.params.username,
					},
				attributes: ['id'],
			})
			.then(account =>
			{
				if(!account)
				{
					return res.status(400).json(
						{
							'Error':
							{
								'Code': 400,
								'Messages': [
									{
										'msg': 'Account not found',
									},
								],
							},
						});
				}
				db.tweet.findAll(
					{
						where:
							{
								accountId: account.id,
							},
						include: [
							{
								model: db.account,
								attributes: ['username', 'name', 'verified', 'avatarurl'],
							}],
						order:
							[
								[ 'id', 'DESC' ],
							],
						attributes: ['id', 'text', 'createdAt'],
					})
					.then(tweets =>
					{
						res.status(200).json(
							{
								'Success':
									{
										'Code': 200,
										'Data': tweets,
									},
							});
					});
			});
	});
};