const SessionManager = require('../../utils/sessionManager');

module.exports = (app, db) =>
{
	app.get('/unfollow/:userid?', async (req, res) =>
	{
		const sessionManager = new SessionManager(db, req.header('token'));
		if(await sessionManager.checkSession() === false)
			return sessionManager.sendIncorrectOrMissingToken(res);

		if(await sessionManager.updateSession() === false)
			return sessionManager.sendInternalServerError(res);

		if(!req.params.userid)
		{
			return res.status(400).json(
				{
					'Error':
					{
						'Code': 400,
						'Messages': [
							{
								'msg': 'Missing parameter userid',
							},
						],
					},
				});
		}
		if(isNaN(parseInt(req.params.userid)))
		{
			return res.status(400).json(
				{
					'Error':
					{
						'Code': 400,
						'Messages': [
							{
								'msg': 'The given userid is not a number',
							},
						],
					},
				});
		}
		const myId = await sessionManager.getSessionUserId();
		if(myId == req.params.userid)
		{
			return res.status(400).json(
				{
					'Error':
					{
						'Code': 400,
						'Messages': [
							{
								'msg': 'You can\'t unfollow yourself ;)',
							},
						],
					},
				});
		}
		db.follower.findOne(
			{
				where:
				{
					followeeId: myId,
					followerId: req.params.userid,
				},
			})
			.then((followed) =>
			{
				if(!followed)
				{
					return res.status(400).json(
						{
							'Error':
							{
								'Code': 400,
								'Messages': [
									{
										'msg': 'You\'re not following this person',
									},
								],
							},
						});
				}
				else
				{
					followed.destroy()
						.then((data) =>
						{
							res.status(200).json(
								{
									'Success':
									{
										'Code': 200,
										'Data': data,
									},
								});
						});
				}
			});
	});
};