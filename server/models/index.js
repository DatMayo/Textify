const fs = require('fs');
const path = require('path');
const Sequelize = require('sequelize');
const basename = path.basename(__filename);
const Logger = require('../utils/logger');
const db = {};
const sequelize = new Sequelize(process.env.DB_DB, process.env.DB_USER, process.env.DB_PASS,
	{
		host: process.env.DB_HOST,
		dialect: process.env.DB_DIALECT,
		logging: (process.env.DB_LOGGING == 'true'),
		pool:
		{
			max: 5,
			min: 0,
			acquire: 30000,
			idle: 10000,
		},
	});
Logger.log('Invoking models - Start', 'debug');
fs
	.readdirSync(__dirname)
	.filter(file =>
	{
		return (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js');
	})
	.forEach(file =>
	{
		Logger.log(path.join(__dirname, file), 'debug');
		const model = sequelize['import'](path.join(__dirname, file));
		db[model.name] = model;
	});
Logger.log('Invoking models - End', 'debug');
Logger.log('Invoking model-links - Start', 'debug');
Object.keys(db).forEach(modelName =>
{
	Logger.log(db[modelName].associate, 'debug');
	if(db[modelName].associate)
		db[modelName].associate(db);
});
Logger.log('Invoking model-links - End', 'debug');

db.sequelize = sequelize;
db.Sequelize = Sequelize;

module.exports = db;