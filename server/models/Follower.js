module.exports = (sequelize, DataTypes) =>
{
	const Follower = sequelize.define('follower',
		{
			id: {
				type: DataTypes.INTEGER,
				primaryKey: true,
				autoIncrement: true,
			},

		});
	Follower.associate = (models) =>
	{
		Follower.belongsTo(models.account, { as: 'followee' });
		Follower.belongsTo(models.account, { as: 'follower' });
	};

	return Follower;
};